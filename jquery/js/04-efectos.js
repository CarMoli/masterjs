'use strict'

$(function(){
	var caja = $('#caja');
	var mostrar = $('#mostrar');
	var ocultar = $('#ocultar');

	mostrar.hide();

	mostrar.click(function(){
		$(this).hide();
		ocultar.show();
		caja.slideDown('slow');
	});
	ocultar.click(function(){
		$(this).hide();
		mostrar.show();
		caja.slideUp('slow', function(){
			console.log('Cartel ocultado');
		});
	});

	$('#allinone').click(function(){
		caja.slideToggle('fast');
	});

	$('#animar').click(function(){
		caja.animate({
				marginLeft: '500px',
				fontSize: '40px',
				height: '100px'
			}, 'slow')
			.animate({
				borderRadius: '900px',
				marginTop: '80px'
			}, 'slow')
			.animate({
				borderRadius: '0px',
				marginLeft: '0px'
			}, 'slow')
			.animate({
				borderRadius: '100px',
				marginTop: '0px'
			}, 'slow')
			.animate({
				marginLeft: '500px',
				fontSize: '40px',
				height: '100px'
			}, 'slow');
	});
});
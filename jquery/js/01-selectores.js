'use strict'

$(function(){
	var p2 = $("#p1");
	$("button").click(function(){
		$("p").hide();
	});
	console.log(p2);

	//Selector ID
	$("#p1").css("background-color", "#99ffff").css("color", "#52335c");
	
	//Selector de clase
	$(".zebra").css("background-color", "#ff6666").css("color", "#0ff");
	
	$('.sinBorde').click(function(){
		console.log("Click dado");
		$(this).addClass('zebra');
	});

	//Selector de etiqueta
	var parrafos = $('p').css('cursor', 'pointer');

	parrafos.click(function(){
		var that = $(this);
		if(!that.hasClass('grande')){
			that.addClass('grande');
		}
		else{
			that.removeClass('grande');
		}
	});

	//Selector de etiqueta
	$('[title="Google"]').css('background', '#ccc');
	$('[title="Facebook"]').css('background', '#0f0');

	//Otros
	//$('p, a').addClass('margenSuperior');

	var busqueda = $('#caja').eq(0).parent().parent().find('[title="Google"]');
	console.log(busqueda);

});


'use strict'

window.addEventListener('load', function(){
  // Timers
  var stop = document.querySelector("#stop");
  var start = document.querySelector("#start");
  var tiempo;

  function intervalo(){
    // Sólo se ejecuta una vez con setTimeout
    //var tiempo = setTimeout(function(){
    var tiempo = setInterval(function(){
      var encabezado = document.querySelector("h2");

      console.log("Set interval ejecutado");

      if(encabezado.style.fontSize == "50px"){
	encabezado.style.fontSize = "20px";
      }else{
	encabezado.style.fontSize = "50px";
      }
    }, 1000);
    
    return tiempo;
  
  } // Final de function intervalo()

  tiempo = intervalo();

  stop.addEventListener('click', function(){
    alert("Has parado el intervalo en bucle");
    clearInterval(tiempo);
  });
  start.addEventListener('click', function(){
    alert("Has iniciado el intervalo en bucle");
    intervalo();
  });

});

'use strict';

// Comprobacion de localstorage
if (typeof (Storage) != 'undefined'){
    console.log('LocalStorage disponible');
}
else{
    console.log('LocalStorage NO disponible');
}

// Guardar datos
localStorage.setItem("titulo", "Curso de Symfony");

// Recuperar elemento
document.querySelector("#peliculas").innerHTML = localStorage.getItem("titulo");

// Guardar objetos
let usuario = {
  nombre: "Car",
  email: "car@algo.com",
  web: "algo.com"
};

localStorage.setItem("usuario", JSON.stringify(usuario));

// Recuperar objeto
let usueJs = JSON.parse(localStorage.getItem("usuario"));

console.log(usueJs);
document.querySelector("#datos").append(usueJs.web + " - " + usueJs.email);

// Eliminar del localStorage
localStorage.removeItem("usuario");
localStorage.clear();

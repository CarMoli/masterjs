'use strict'

//PARAMETROS REST Y SPREAD

listadoFrutas("Naranja", "Manzana","Sandia","Pera","Coco","Tuna");
var frutas=["Limón","Aguacate"];
listadoFrutas(...frutas,"Naranja", "Manzana","Sandia","Pera","Coco","Tuna");

function listadoFrutas(fruta1, fruta2, ...resto_de_frutas){
	console.log("Fruta 1: ", fruta1);
	console.log("Fruta 2: ", fruta2);
	console.log("Frutas: ", resto_de_frutas);
}
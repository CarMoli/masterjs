'use strict'

var nombres = ["Carlos Molina", "Victor Robles", "Juan Gutiérrez", 52, true];
var lenguajes = new Array("PHP", "JS", "Go", "Java", "C");
var person = {
  firstname:"John", 
  lastName:"Doe",
  age:46
};
var busqueda;
var precios = [10, 20, 50, 12, 100];

/*
console.log(nombres);
console.log(lenguajes);
console.log(nombres.length);
console.log(person.firstname);
*/

/*
var elemento = parseInt(prompt("Que elemento del array quieres?", 0));
if(elemento>= nombres.length || elemento<0){
    alert("Introduce el numero correcto menor que "+nombres.length+" y mayor que 0.");
}else{
    alert("El usuario seleccionado es "+nombres[elemento]);
}
*/
document.write("<h1>Lenguajes de programacion del 2018</h1>");
document.write("<ul>");
/*
for(var i=0; i<lenguajes.length; i++){
    document.write("<li>"+lenguajes[i]+"</li>");
}
*/

// Estructura de un forEach
// forEach((valorActual, indice, arreglo)=>{ sentencias });
/*
lenguajes.forEach( (elemento, index, data)=>{
	document.write("<li>"+index+" "+elemento+"</li>");
});
*/
for(let lenguaje in lenguajes){
  document.write("<li>"+ lenguajes[lenguaje] +"</li>");
}

document.write("</ul>");

// Busquedas

// busqueda = lenguajes.findIndex( lenguaje => lenguaje == "Go" );
busqueda = precios.some( precio => precio >= 20 );

console.log(busqueda);

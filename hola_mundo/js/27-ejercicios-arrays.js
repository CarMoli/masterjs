'use strict'

/*
1. Pida 6 numeros por pantalla y los meta en un array
2. Mostrar el array entero (todos sus elementos) en
el cuerpo de la pagina y en la consola
3. Ordenarlo y mostrarlo
4. Invertir su orden y mostrarlo
5. Mostrar cuantos elementos tiene el array
6. Busqueda de un valor introducido por el usuario,
que nos diga si lo encuentra y su indice 
(Se valorará el  uso de funciones)
*/


function buscar(numeros, busqueda, valor=false){
  let resultado;
  for(let numero in numeros){
    if(busqueda === numeros[numero]){
      resultado = "<strong>El número " + busqueda + " ha sido encontrado, su posición es: " + numero + ".</strong>";
      break;
    }else{
      resultado = "El numero no existe";
    }
  }
  document.write(resultado);
}

function imprime(arreglo, tipo="Arreglo"){
  document.write("<h2>" + tipo + "</h2>");
  document.write("<ul>");
  for(let numero in numeros){
    document.write("<li>" + numeros[numero] + "</li>");
  }
  document.write("</ul>");
}

var numeros = [];
var i;
var numLen, tipo, busqueda, valor;

for(i=0; i<=5; i++){
  numeros.push(parseInt(prompt("Ingresa 6 números", 0)));
}
imprime(numeros);

console.log(numeros);

numeros.sort(function(a,b){return a-b});
tipo = "Ordenados";
imprime(numeros, tipo);

numeros.reverse();
tipo = "Invertidos";
imprime(numeros, tipo);

document.write("<h4>El arreglo tiene: " + numeros.length + " elementos</h4>");

busqueda = parseInt(prompt("¿Qué número quieres buscar?"));
buscar(numeros, busqueda);



'use strict'

var categorias = ['Accion', 'Terror', 'Comedia'];
var peliculas = ['Your name', 'IT', 'El camino'];
var cine = [categorias, peliculas];
var elemento = "";
var indice;
var pelisEnString;
var cadena;
var cadena_array;

/*
do{
  elemento = prompt("Agrega una peli.\nEscribe ACABAR cuando finalices.");
  peliculas.push(elemento);
}while(elemento != "ACABAR");

peliculas.pop();
*/

peliculas.reverse(); // Ordena en orden inverso
console.log(peliculas);
peliculas.sort(); // Ordena en orden alfabetico
console.log(peliculas);

indice = peliculas.indexOf('Your name');
console.log(indice);
if(indice > -1){
  peliculas.splice(indice, 1);
}

pelisEnString = peliculas.join();
cadena = "texto1, texto2, texto3";

cadena_array = cadena.split(", ");
console.log(cadena_array);

console.log(typeof pelisEnString);
